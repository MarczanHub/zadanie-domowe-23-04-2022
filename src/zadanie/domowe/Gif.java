package zadanie.domowe;

public class Gif {

    // Stw�rz klas� Gif, kt�ra przechowuje informacj� o obrazkach:
    // - nazwa
    // - url obrazka
    // - opis
    // - czy jest ulubiony
    // Url obrazka musi by� poprawnym adresem url. Stw�rz przyk�adow� list� Mem�w.
    // Wy�wietl tylko te memy, kt�re s� ulubione.

    boolean isFavorite;
    String name;
    String imageURL;
    String description;

        public Gif(boolean isFavorite, String name, String imageURL, String description) {
        this.isFavorite = isFavorite;
        this.name = name;
        this.imageURL = imageURL;
        this.description = description;
    }
    public void getDescription(){

        System.out.println("Name of meme: " + name);
        System.out.println("Image URL: " + imageURL);
        System.out.println("Description: " + description);
        System.out.println("Favorite status: " + isFavorite);

    }
}
