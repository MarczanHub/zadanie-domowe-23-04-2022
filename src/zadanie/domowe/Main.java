package zadanie.domowe;

public class Main {
    public static void main(String[] args) {

        Gif meme1 = new Gif(true, "Picture nr 1", "https://codingbootcamps.io/wp-content/uploads/m2.png", "'Designers vs Programmers'");
        Gif meme2 = new Gif(false, "Picture nr 2", "https://funvizeo.com/media/memes/6e4800437a822d63/what-happened-triedtc-learn-data-styuctlipes-cporittimac-fafba99835b071f0-31b315bf4e085f10.jpg", "'Data Structures and Algorithms in Java'");
        Gif meme3 = new Gif(true, "Picture nr 3", "https://img-9gag-fun.9cache.com/photo/aEgeLgx_700bwp.webp", "'Does he bite?'");
        Gif meme4 = new Gif(true, "Picture nr 4", "https://img-9gag-fun.9cache.com/photo/aqGWyMv_700bwp.webp", "'CS background'");
        Gif meme5 = new Gif(false, "Picture nr 5", "https://img-9gag-fun.9cache.com/photo/avAvxwb_460swp.webp", "'I have no idea...'");

        Gif[] gifs = new Gif[5];
        gifs[0] = meme1;
        gifs[1] = meme2;
        gifs[2] = meme3;
        gifs[3] = meme4;
        gifs[4] = meme5;

        for (Gif gif : gifs) {
            if (gif.isFavorite) {
                System.out.println("This meme is my favorite");
                gif.getDescription();
            } else {
                continue;
            }
            System.out.println("-----------------------------------");
        }

    }
}
